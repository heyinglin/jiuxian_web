<?php
include "../../header.php";
?>
<!--购物车页面的实现-->
<div class="cart">
	<!--第一部分-->
	<!--广告部分-->
	<div class="dTop">
		<a href="javascript:;">
			<img src="../../public/img/264b0c2b552d4023a66735951fac236d.jpg" alt="">
		</a>
	</div>
	<!--导航部分-->
	<div class="dGuide">
		<a href="javascript:;">首页</a>
		<i>></i>
		<a href="javascript:;">葡萄酒</a>
		<i>></i>
		<a href="javascript:;">法圣古堡</a>
		<i>></i>
		<span>法国(原瓶进口)法圣古堡圣威骑士干红葡萄酒750ml(6瓶装)</span>
	</div>
	<!--商品展示部分-->
	<div class="dIntro">
		<!--左边部分-->
		<div class="introShow">
			<!--上-->
			<div class="showPic">
				<img src="../../public/img/ffef0e6533c745a290983c1d8fe2f69a5.jpg" alt="">
			</div>
			<!--中-->
			<div class="showList clearfix">
				<a href="" class="show-list-prev">
					<i class="dIcon"></i>
				</a>
				<div class="show-list-con clearfix">
					<ul>
						<li>
							<a href=""><img src="../../public/img/ffef0e6533c745a290983c1d8fe2f69a1.jpg" alt="">
							</a>
						</li>
						<li>
							<a href=""><img src="../../public/img/c24a2f0bf66b4e6fa4d581700ba6504f1.jpg" alt="">
							</a>
						</li>
						<li>
							<a href=""><img src="../../public/img/46a6eac5c09242b1adcbaaa21fbb1e421.jpg" alt="">
							</a>
						</li>
						<li>
							<a href=""><img src="../../public/img/306d887c2579466ea8d0d3b500a8686d1.jpg" alt="">
							</a>
						</li>
					</ul>
				</div>
				<a href="" class="show-list-next clearfix">
					<i class="dIcon"></i>
				</a>
			</div>
			<!--下-->
			<div class="showOther clearfix">
				<div class="goodsNum">
					<span>
						商品编号：tz0010541
					</span>
				</div>
				<div class="sOtherRight clearfix">
					<div class="showCollect">
						<a href="javascript:;">
							<i class="dIcon"></i>
							<span>收藏(0)</span>
						</a>
					</div>
					<div class="showShare">
						<a href="javascript:;">
							<i class="dIcon"></i>
							<span>分享</span>
						</a>
					</div>
				</div>
			</div>
		</div>	<!-- /.introShow-->
		<!--中间部分-->
		<div class="introInfo">
			<div class="comName">
				<h1>
					<i class="zhenxuan"></i>
					法国(原瓶进口)法圣古堡圣威骑士干红葡萄酒750ml(6瓶装)
				</h1>
				<p>法国原装进口,热销爆款,补货归来</p>
			</div>
			<div class="price_and_promo">
				<div class="infoItem">
					<div class="infoTit">酒仙价</div>
					<div class="nowPrice"><span>￥99.90</span></div>
					<div class="mobileBay">
					<p>
						<i class="dIcon"></i>
						<span>手机购买</span>
						<em class="dIcon"></em>
					</p>
					</div>
				</div>
				<div class="clubDisc">
					<i class="clubImg"></i>
					<span>会员下单再享99折,可省0.99元</span>
					<a href="javascript:;">开通会员></a>
				</div>
				<div class="promotionInfo">
					<span class="infoTit">促销</span>
					<span class="nowPrice">订单加价购</span>
					<span class="adverTise">满299元加9.9元得小M1</span>
				</div>
			</div>
			<div class="salesScoreGold">
				<ul>
					<li class="comSales">
						<i class="dIcon"></i>
						<span>累计销量</span>
						<em>18078</em>
					</li>
					<li class="comScore">
						<i class="dIcon"></i>
						<span>酒友评分</span>
						<em>4.9</em>
					</li>
					<li class="comGold">
						<i class="dIcon"></i>
						<span>送金币</span>
						<em>49</em>
					</li>
				</ul>
			</div>
			<div class="infoWrap">
				<div class="cityBox">
					<span class="infoTit">配送到</span>
					<div class="citySelect">
						<a href="javascript:;" class="dOrder-city-sel">
							<span>北京</span>
							<i class="dIcon"></i>
						</a>
					</div>
					<strong>有货</strong>
				</div>
				<div class="chooseBox"></div>
				<div class="bayNumBox">
					<span class="infoTit">数量</span>
					<div class="infoBuyNum">
						<input type="text" class="" value="1">
						<a href="" class="top-btn"></a>
						<a href="" class="bottom-btn"></a>
					</div>
					<span class="buyPro">此商品无原品手提袋</span>
				</div>
				<div class="bayBtnBox">
					<a href="" class="buyBtnCart">加入购物车</a>
				</div>
				<div class="promptBox"></div>
			</div>
		</div>	<!-- /.introInfo-->
		<!--右边部分-->
		<div class="introOther">
			<div class="introLogo">
				<a href="">
					<img src="../../public/img/1b6be4d42f3949e8b70aaa9461be73d2.jpg" alt="">
				</a>
				<span>奔富</span>
				<i class="jxzy">酒仙自营</i>
			</div>
			<div class="introSaf">
				<ul>
					<li>
						<p class="safNam">
							<img src="../../public/img/3d43576517d54bccb50f3303c01a746e.png" alt="">
							<strong>退货无忧</strong>
						</p>
						<p class="safCon">7天无理由退货</p>
					</li>
					<li>
						<p class="safNam">
							<img src="../../public/img/3d43576517d54bccb50f3303c01a746e.png" alt="">
							<strong>售后补充</strong>
						</p>
						<p class="safCon">啤/老酒无理由不退换</p>
					</li>
					<li>
						<p class="safNam">
							<img src="../../public/img/3d43576517d54bccb50f3303c01a746e.png" alt="">
							<strong>满100包邮</strong>
						</p>
						<p class="safCon">全场满100包邮 </p>
					</li>
					<li>
						<p class="safNam">
							<img src="../../public/img/3d43576517d54bccb50f3303c01a746e.png" alt="">
							<strong>正品保障</strong>
						</p>
						<p class="safCon">厂家直供 品质放心</p>
					</li>
				</ul>
			</div>
			<div class="kefuDetail">
				<a href="">
					<i class="dIcon"></i>
					<span>在线侍酒师</span>
				</a>
			</div>
		</div>	<!-- /.introOther-->
	</div>
	<!--第二部分-->
	<div class="setWrap">
		<div class="setTab">
			<div class="item">
				<span>推荐组合</span>
			</div>
		</div>
		<div class="setConWrap">
			<div class="setBox">
				<div class="comBox">
					<div class="master">
						<div class="mImg">
							<a href="">
								<img src="../../public/img/0d24c88a779f4f3285765b03cb877ad62.jpg" alt="">
							</a>
						</div>
						<div class="mName">
							<div class="mTic">
							<a href="">澳大利亚奔富BIN2西拉马塔罗干红葡萄酒750ml</a>
							</div>
						</div>
					</div>
					<div class="vice">
						<ul>
						<li>
							<div class="m-img">
								<a href=""><img src="../../public/img/3c286e1f18634b46a158d243c605c9492.jpg" alt="">
								</a>
							</div>
							<div class="m-name">
								<a href="">
									52°汾酒集团 汾酒原浆 汾牌1915藏品奢华礼盒 清香型白酒475ml*6瓶
								</a>
							</div>
							<div class="m-price">
								<label for="">
									<input type="checkbox" name="group" value="">
									<span>￥</span>
									<span>388.00</span>
								</label>
							</div>
						</li>
						<li>
							<div class="m-img">
								<a href=""><img src="../../public/img/3c286e1f18634b46a158d243c605c9492.jpg" alt="">
								</a>
							</div>
							<div class="m-name">
								<a href="">
									52°汾酒集团 汾酒原浆 汾牌1915藏品奢华礼盒 清香型白酒475ml*6瓶
								</a>
							</div>
							<div class="m-price">
								<label for="">
									<input type="checkbox" name="group" value="">
									<span>￥</span>
									<span>388.00</span>
								</label>
							</div>
						</li>
						<li>
							<div class="m-img">
								<a href=""><img src="../../public/img/3c286e1f18634b46a158d243c605c9492.jpg" alt="">
								</a>
							</div>
							<div class="m-name">
								<a href="">
									52°汾酒集团 汾酒原浆 汾牌1915藏品奢华礼盒 清香型白酒475ml*6瓶
								</a>
							</div>
							<div class="m-price">
								<label for="">
									<input type="checkbox" name="group" value="">
									<span>￥</span>
									<span>388.00</span>
								</label>
							</div>
						</li>
						<li>
							<div class="m-img">
								<a href=""><img src="../../public/img/3c286e1f18634b46a158d243c605c9492.jpg" alt="">
								</a>
							</div>
							<div class="m-name">
								<a href="">
									52°汾酒集团 汾酒原浆 汾牌1915藏品奢华礼盒 清香型白酒475ml*6瓶
								</a>
							</div>
							<div class="m-price">
								<label for="">
									<input type="checkbox" name="group" value="">
									<span>￥</span>
									<span>388.00</span>
								</label>
							</div>
						</li>
						<li>
							<div class="m-img">
								<a href=""><img src="../../public/img/3c286e1f18634b46a158d243c605c9492.jpg" alt="">
								</a>
							</div>
							<div class="m-name">
								<a href="">
									52°汾酒集团 汾酒原浆 汾牌1915藏品奢华礼盒 清香型白酒475ml*6瓶
								</a>
							</div>
							<div class="m-price">
								<label for="">
									<input type="checkbox" name="group" value="">
									<span>￥</span>
									<span>388.00</span>
								</label>
							</div>
						</li>
						<li>
							<div class="m-img">
								<a href=""><img src="../../public/img/3c286e1f18634b46a158d243c605c9492.jpg" alt="">
								</a>
							</div>
							<div class="m-name">
								<a href="">
									52°汾酒集团 汾酒原浆 汾牌1915藏品奢华礼盒 清香型白酒475ml*6瓶
								</a>
							</div>
							<div class="m-price">
								<label for="">
									<input type="checkbox" name="group" value="">
									<span>￥</span>
									<span>388.00</span>
								</label>
							</div>
						</li>
						<li>
							<div class="m-img">
								<a href=""><img src="../../public/img/3c286e1f18634b46a158d243c605c9492.jpg" alt="">
								</a>
							</div>
							<div class="m-name">
								<a href="">
									52°汾酒集团 汾酒原浆 汾牌1915藏品奢华礼盒 清香型白酒475ml*6瓶
								</a>
							</div>
							<div class="m-price">
								<label for="">
									<input type="checkbox" name="group" value="">
									<span>￥</span>
									<span>388.00</span>
								</label>
							</div>
						</li>
						<li>
							<div class="m-img">
								<a href=""><img src="../../public/img/3c286e1f18634b46a158d243c605c9492.jpg" alt="">
								</a>
							</div>
							<div class="m-name">
								<a href="">
									52°汾酒集团 汾酒原浆 汾牌1915藏品奢华礼盒 清香型白酒475ml*6瓶
								</a>
							</div>
							<div class="m-price">
								<label for="">
									<input type="checkbox" name="group" value="">
									<span>￥</span>
									<span>388.00</span>
								</label>
							</div>
						</li>
						<li>
							<div class="m-img">
								<a href=""><img src="../../public/img/3c286e1f18634b46a158d243c605c9492.jpg" alt="">
								</a>
							</div>
							<div class="m-name">
								<a href="">
									52°汾酒集团 汾酒原浆 汾牌1915藏品奢华礼盒 清香型白酒475ml*6瓶
								</a>
							</div>
							<div class="m-price">
								<label for="">
									<input type="checkbox" name="group" value="">
									<span>￥</span>
									<span>388.00</span>
								</label>
							</div>
						</li>
						<li>
							<div class="m-img">
								<a href=""><img src="../../public/img/3c286e1f18634b46a158d243c605c9492.jpg" alt="">
								</a>
							</div>
							<div class="m-name">
								<a href="">
									52°汾酒集团 汾酒原浆 汾牌1915藏品奢华礼盒 清香型白酒475ml*6瓶
								</a>
							</div>
							<div class="m-price">
								<label for="">
									<input type="checkbox" name="group" value="">
									<span>￥</span>
									<span>388.00</span>
								</label>
							</div>
						</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="accBox">
				<div class="total">
					已选择
					<strong>0</strong>
					商品
				</div>
				<p>
					<span>搭配价：</span>
					<strong>￥199.00</strong>
				</p>
				<a href="" class="setBuy">购买套装</a>
			</div>
		</div>
	</div>
	<!--第三部分-->
	<div class="d">
		<div class="d-left">
			<div class="d-box1 d-box">
				<div class="title">
					<i class="dIcon"></i>
					<span>相关分类</span>
				</div>
				<div class="d-con" name="__detail_xiangguanfenlei">
					<div class="twoList">
						<ul class="clearfix">
							<li><a href="">白酒</a></li>
							<li><a href="">葡萄酒</a></li>
							<li><a href="">洋酒</a></li>
							<li><a href="">黄酒</a></li>
							<li><a href="">养生酒</a></li>
							<li><a href="">啤酒</a></li>
							<li><a href="">果酒</a></li>
							<li><a href="">酒具</a></li>
						</ul>
					</div>
				</div>
			</div>	<!--/ .d-box1-->
			<div class="d-box2 d-box">
				<div class="title">
					<i class="dIcon"></i>
					<span>推荐品牌</span>
				</div>
				<div class="d-con" name="__detail_xiangguanfenlei">
					<div class="twoList">
						<ul class="clearfix">
							<li><a href="">拉菲</a></li>
							<li><a href="">奔富酒园</a></li>
							<li><a href="">长城</a></li>
							<li><a href="">黄尾袋鼠</a></li>
							<li><a href="">詹姆士</a></li>
							<li><a href="">尼雅葡...</a></li>
							<li><a href="">张裕</a></li>
							<li><a href="">红魔鬼</a></li>
							<li><a href="">智利人</a></li>
						</ul>
					</div>
				</div>
			</div>	<!--/ .d-box2-->
			<div class="d-box3 d-box">
				<div class="title">
					<i class="dIcon"></i>
					<span>酒友推荐</span>
				</div>
				<div class="d-con" name="__detail_xiangguanfenlei">
					<div class="recomm_direct_2">
						<ul class="clearfix">
							<li>
								<div class="pic">
									<a href="">
										<img src="../../public/img/9a0729cdcac94714ae9c034bb3f4f05d2.jpg" alt="">
									</a>
								</div>
								<div class="name"><a href="">西班牙安徒生·小天鹅干红葡萄酒750ml（6瓶装）</a></div>
								<div class="price"><p>￥99.00</p></div>
							</li>
							<li>
								<div class="pic">
									<a href="">
										<img src="../../public/img/9a0729cdcac94714ae9c034bb3f4f05d2.jpg" alt="">
									</a>
								</div>
								<div class="name"><a href="">西班牙安徒生·小天鹅干红葡萄酒750ml（6瓶装）</a></div>
								<div class="price"><p>￥99.00</p></div>
							</li>
							<li>
								<div class="pic">
									<a href="">
										<img src="../../public/img/9a0729cdcac94714ae9c034bb3f4f05d2.jpg" alt="">
									</a>
								</div>
								<div class="name"><a href="">西班牙安徒生·小天鹅干红葡萄酒750ml（6瓶装）</a></div>
								<div class="price"><p>￥99.00</p></div>
							</li>
							<li>
								<div class="pic">
									<a href="">
										<img src="../../public/img/9a0729cdcac94714ae9c034bb3f4f05d2.jpg" alt="">
									</a>
								</div>
								<div class="name"><a href="">西班牙安徒生·小天鹅干红葡萄酒750ml（6瓶装）</a></div>
								<div class="price"><p>￥99.00</p></div>
							</li>
							<li>
								<div class="pic">
									<a href="">
										<img src="../../public/img/9a0729cdcac94714ae9c034bb3f4f05d2.jpg" alt="">
									</a>
								</div>
								<div class="name"><a href="">西班牙安徒生·小天鹅干红葡萄酒750ml（6瓶装）</a></div>
								<div class="price"><p>￥99.00</p></div>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="d-box4 d-box">
				<div class="title">
					<i class="dIcon"></i>
					<span>排行榜</span>
				</div>
				<div class="d-con" name="__detail_xiangguanfenlei">
					<div class="topTen_box">
						<div class="topTenTab">
							<ul class="clearfix">
								<li class="cur">同价位</li>
								<li>同品牌</li>
								<li>同类别</li>
							</ul>
						</div>
						<div class="topTenWrap">
							<ul class="topTenList clearfix">
								<li>
									<i class="dIcon">1</i>
									<div class="pic">
										<a href="">
											<img src="../../public/img/ffef0e6533c745a290983c1d8fe2f69a2.jpg" alt="" width="60px" height="60px">
										</a>
									</div>
									<div class="right">
										<div class="name">
											<a href="">法国(原瓶进口)法圣古堡圣威骑士干红葡萄酒750ml(6瓶装)
											</a>
										</div>
										<div class="price">
											<p>￥99.00</p>
										</div>
									</div>
								</li>
								<li>
									<i class="dIcon">2</i>
									<div class="pic">
										<a href="">
											<img src="../../public/img/ffef0e6533c745a290983c1d8fe2f69a2.jpg" alt="" width="60px" height="60px">
										</a>
									</div>
									<div class="right">
										<div class="name">
											<a href="">法国(原瓶进口)法圣古堡圣威骑士干红葡萄酒750ml(6瓶装)
											</a>
										</div>
										<div class="price">
											<p>￥99.00</p>
										</div>
									</div>
								</li>
								<li>
									<i class="dIcon">3</i>
									<div class="pic">
										<a href="">
											<img src="../../public/img/ffef0e6533c745a290983c1d8fe2f69a2.jpg" alt="" width="60px" height="60px">
										</a>
									</div>
									<div class="right">
										<div class="name">
											<a href="">法国(原瓶进口)法圣古堡圣威骑士干红葡萄酒750ml(6瓶装)
											</a>
										</div>
										<div class="price">
											<p>￥99.00</p>
										</div>
									</div>
								</li>
								<li>
									<i class="dIcon">4</i>
									<div class="pic">
										<a href="">
											<img src="../../public/img/ffef0e6533c745a290983c1d8fe2f69a2.jpg" alt="" width="60px" height="60px">
										</a>
									</div>
									<div class="right">
										<div class="name">
											<a href="">法国(原瓶进口)法圣古堡圣威骑士干红葡萄酒750ml(6瓶装)
											</a>
										</div>
										<div class="price">
											<p>￥99.00</p>
										</div>
									</div>
								</li>
								<li>
									<i class="dIcon">5</i>
									<div class="pic">
										<a href="">
											<img src="../../public/img/ffef0e6533c745a290983c1d8fe2f69a2.jpg" alt="" width="60px" height="60px">
										</a>
									</div>
									<div class="right">
										<div class="name">
											<a href="">法国(原瓶进口)法圣古堡圣威骑士干红葡萄酒750ml(6瓶装)
											</a>
										</div>
										<div class="price">
											<p>￥99.00</p>
										</div>
									</div>
								</li>
								<li>
									<i class="dIcon">6</i>
									<div class="pic">
										<a href="">
											<img src="../../public/img/ffef0e6533c745a290983c1d8fe2f69a2.jpg" alt="" width="60px" height="60px">
										</a>
									</div>
									<div class="right">
										<div class="name">
											<a href="">法国(原瓶进口)法圣古堡圣威骑士干红葡萄酒750ml(6瓶装)
											</a>
										</div>
										<div class="price">
											<p>￥99.00</p>
										</div>
									</div>
								</li>
							</ul>
							<ul></ul>
							<ul></ul>
						</div>
					</div>
				</div>
			</div>
			<div class="d-box5 d-box"></div>
		</div>
		<div class="d-right"></div>
	</div>
</div>	<!-- /.cart-->