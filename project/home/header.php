<?php
$path=$_SERVER['PHP_SELF'];
$arr=explode('/',$path);
$root='/'.$arr[1].'/'.$arr[2].'/';
// echo $root;exit;
?>
<!doctype html>
<html>
<head>
	<meta charset="UTF-8">
	<title>酒仙网-白酒、红酒、洋酒、保健酒、黄酒、酒具官方旗舰店</title>	<!--百度搜索引擎收录时用到-->
	<meta name="Keywords" content="酒仙,酒仙网,酒仙网官方网站,酒仙网官方旗舰店" />	<!--百度搜索引擎收录时用到-->
    <meta name="Description" content="【酒仙网】酒水在线销售专业品牌，提供白酒、红酒、洋酒、保健酒、黄酒、酒具、正品等多种品类，官方授权在线销售，各类酒水团购、秒杀不断。" />
   <!--  <link rel="stylesheet" href="../public/css/reset.css"> -->
	<link rel="stylesheet" href="<?php echo $root?>home/public/css/index.css">
	<link rel="stylesheet" href="<?php echo $root?>home/public/css/cart.css">
	<link rel="stylesheet" href="<?php echo $root?>home/public/css/common.css">
	<script src="<?php echo $root?>home/public/js/jquery.min.js"></script>
	<script src="<?php echo $root?>home/public/js/header.js"></script>
</head>
<body>
	<!--顶部导航开始-->
	<div class="top">
		<div class="top-main">
			<div class="main-left">
				<span>欢迎来到酒仙网</span>
				<a href="#">请登录</a>
				<a href="#">免费注册</a>
			</div>
			<div class="main-right">
				<div class="jiuxian">
					<div class="jiuxiantop">
						<span>我的酒仙 <i class="caret"></i></span>	<!--小空格是为了隔开一点-->
					</div>
					<div class="mydropdown">
						<ul>
							<li><a href="">我的订单</a></li>
							<li><a href="">物流跟踪</a></li>
							<li><a href="">我的优惠券</a></li>
						</ul>
					</div>
				</div>
				<a href="">我的订单</a>
				<a href="">物流跟踪</a>
				<a href="">我的优惠券</a>
				<a href="" class='ceo'>CEO邮箱</a>
				<a href=""><i class="cart"></i>购物车0件</a>	<!--此处不能用i标签，要换成span标签-->
				<a href="" class='ceo'>社区</a>
				<a href="" class='shouji'><i class="pre"></i>手机逛酒仙</a>
				<a href="">体验店加盟</a>
				<a href=""><i class="web"></i>网站导航</a>
				<a href=""><i class="kefu"></i>客户服务</a>
				<span>客服热线:</span>
				<span class='ceo'>400-617-9999</span>
			</div>
		</div>	<!--/ .top-main-->
	</div>	<!--/ .top-->
	<!--顶部导航结束-->


	<!--第二层的广告部分开始-->
	<div class="banner">
		<img src="<?php echo $root?>home/public/img/b234003cb33743b48d213ab834c50ac8.jpg" alt="">
	</div>
	<!--第二层的广告部分结束-->
	
	<!--第三层的搜索栏部分开始-->
	<div class="midHeader">
		<div class="midHeader-main">
			<div class="logo">
				<img src="<?php echo $root?>home/public/img/logo.png" alt="">
			</div>
			<div class="huodong">
				<img src="<?php echo $root?>home/public/img/1dcea8e15965436baa420ce04c25031f.gif" alt="">
			</div>
			<div class="search">
				<div class="search-top">
					<form action="" class="">
						<input type="text" class="search-form">
						<input type="button" class="search-btn" value="搜索">
					</form>
				</div>
				<div class="search-bottom">
					<ul>
						<li><a href="" class='first'>茅台</a><s class='shuxian'></s></li>
						<li><a href="">五粮液</a><s class='shuxian'></s></li>
						<li><a href="">泸州老窖</a><s class='shuxian'></s></li>
						<li><a href="">啤酒</a><s class='shuxian'></s></li>
						<li><a href="">拉菲</a><s class='shuxian'></s></li>
						<li><a href="">梅多克产区特卖</a><s class='shuxian'></s></li>
						<li><a href="">牛排首发</a></li>
					</ul>	
				</div>
			</div>
			<div class="yunfei"></div>
		</div>	<!-- /.midHeader-main-->
	</div>	<!-- /.midHeader-->
	<!--第三层的搜索栏部分结束-->

	<!--幻灯片上部导航以及左侧边栏的实现-->
	<div class="nav">
		<div class="nav-main">
			<!--左侧边栏的实现开始-->
			<div class="left">
			<h2>全部商品分类</h2>
			<ul class="leftmenu">
				<li class="menuli">
					<!--这个div是为了鼠标放上li标签上的时候,文字不会不会往右移动,配合css和js来实现-->
					<div class="leftnav"></div>
					<div class='leftdiv'>
						<h3 class='menu-top'>
							<i></i>
							<span>一键选酒</span>
						</h3>
						<div class='menu-down'>
							<p class="first">
								<a href="">整箱购</a>
								<a href="">婚宴酒</a>
								<a href="">大坛专区</a>
							</p>
							<p>
								<a href="">礼品酒</a>
								<a href="">年份老酒</a>
								<a href="">各地名酒</a>
							</p>
						</div>
						</div>
					<div class="rightdiv">
						测试文字
					</div>
				</li>
				<li class="menuli">
					<!--这个div是为了鼠标放上li标签上的时候,文字不会不会往右移动,配合css和js来实现-->
					<div class="leftnav"></div>
					<div class='leftdiv'>
						<h3 class='menu-top'>
							<i></i>
							<span>一键选酒</span>
						</h3>
						<div class='menu-down'>
							<p class="first">
								<a href="">整箱购</a>
								<a href="">婚宴酒</a>
								<a href="">大坛专区</a>
							</p>
							<p>
								<a href="">礼品酒</a>
								<a href="">年份老酒</a>
								<a href="">各地名酒</a>
							</p>
						</div>
						</div>
					<div class="rightdiv">
						测试文字
					</div>
				</li>
				<li class="menuli">
					<!--这个div是为了鼠标放上li标签上的时候,文字不会不会往右移动,配合css和js来实现-->
					<div class="leftnav"></div>
					<div class='leftdiv'>
						<h3 class='menu-top'>
							<i></i>
							<span>一键选酒</span>
						</h3>
						<div class='menu-down'>
							<p class="first">
								<a href="">整箱购</a>
								<a href="">婚宴酒</a>
								<a href="">大坛专区</a>
							</p>
							<p>
								<a href="">礼品酒</a>
								<a href="">年份老酒</a>
								<a href="">各地名酒</a>
							</p>
						</div>
						</div>
					<div class="rightdiv">
						测试文字
					</div>
				</li>
				<li class="menuli">
					<!--这个div是为了鼠标放上li标签上的时候,文字不会不会往右移动,配合css和js来实现-->
					<div class="leftnav"></div>
					<div class='leftdiv'>
						<h3 class='menu-top'>
							<i></i>
							<span>一键选酒</span>
						</h3>
						<div class='menu-down'>
							<p class="first">
								<a href="">整箱购</a>
								<a href="">婚宴酒</a>
								<a href="">大坛专区</a>
							</p>
							<p>
								<a href="">礼品酒</a>
								<a href="">年份老酒</a>
								<a href="">各地名酒</a>
							</p>
						</div>
						</div>
					<div class="rightdiv">
						测试文字
					</div>
				</li>
				<li class="menuli">
					<!--这个div是为了鼠标放上li标签上的时候,文字不会不会往右移动,配合css和js来实现-->
					<div class="leftnav"></div>
					<div class='leftdiv'>
						<h3 class='menu-top'>
							<i></i>
							<span>一键选酒</span>
						</h3>
						<div class='menu-down'>
							<p class="first">
								<a href="">整箱购</a>
								<a href="">婚宴酒</a>
								<a href="">大坛专区</a>
							</p>
							<p>
								<a href="">礼品酒</a>
								<a href="">年份老酒</a>
								<a href="">各地名酒</a>
							</p>
						</div>
						</div>
					<div class="rightdiv">
						测试文字
					</div>
				</li>
				<li class="lastli">
					<!--这个div是为了鼠标放上li标签上的时候,文字不会不会往右移动,配合css和js来实现-->
					<div class="leftnav"></div>
					<div class='leftdiv'>
						<h3 class='menu-top'>
							<i></i>
							<span>茶叶</span>
						</h3>
						<div class='menu-down'>
							<p class="first">
								<a href="">整箱购</a>
								<a href="">婚宴酒</a>
								<a href="">大坛专区</a>
							</p>
						</div>
						</div>
					<div class="rightdiv">
						测试文字
					</div>
				</li>
			</ul>
			</div>
			<!--左侧边栏实现结束-->

			<!--幻灯片上方导航部分开始-->
			<div class="center">
				<ul>
					<li class="first"><a href="">首页</a></li>
					<li><a href="">酒仙独家</a></li>
					<li><a href="">葡萄酒馆</a></li>
					<li><a href="">洋酒馆</a></li>
					<li><a href="">团购</a></li>
					<li><a href="">清仓</a></li>
					<li><a href="">秒拍</a></li>
					<li><a href="">新品</a></li>
					<li><a href="">超级单品日</a></li>
				</ul>
			</div>
			
			<div class="right">
				<a href="">
					<img src="<?php echo $root?>home/public/img/b6fe651e140e411eb3fc10873445a068.gif" alt="">
				</a>
			</div>
			<!--幻灯片上方导航的部分结束-->
		</div>	<!-- /.nav-main-->
	</div>	<!-- /.nav-->
	<!--幻灯片上部导航以及左侧边栏的结束-->
	
	<!--右侧导航实现-->
	<div class="rightSideBar">
		<!--上方部分-->
		<div class="rightSideBarCon">
			<!--我-->
			<div class="rSideBarItem user">
				<div class="rsItemName">
					<i class="publicIcon user-i"></i>
					<span>我</span>
				</div>
				<div class="sideBarUserLogin">
					
				</div>
			</div>
			<!--收藏-->
			<div class="rSideBarItem collectSideBar">
				<div class="rsItemName">
					<i class="publicIcon collect-i"></i>
					<span>收藏</span>
				</div>
			<!-- 				<div class="sideBarUserLogin">
	
					</div> -->
			</div>
			<!--购物车-->
			<div class="rSideBarItem cart">
				<div class="rsItemName">
					<i class="publicIcon cart-i"></i>
					<span class="cart-n">购物车</span>
				</div>
				<!-- <div class="sideBarUserLogin">
					
				</div> -->
			</div>
			
			<div class="rSideBarItem onlineService">
				<div class="rsItemName">
					<i class="publicIcon online-i"></i>
					<span class="online-n">客服</span>
				</div>
				<!-- <div class="sideBarUserLogin">
					
				</div> -->
			</div>
		</div>
		<!--下方部分-->
	   <div class="rightSideBarBot">
	   	<!--用户反馈-->
			<div class="rSideBarItem feedback">
				<div class="rsItemName">
					<i class="publicIcon feedback-i"></i>
				</div>
				<!-- <div class="sideBarUserLogin">
					
				</div> -->
			</div>
			<!--二维码部分-->
			<div class="rSideBarItem code">
				<div class="rsItemName">
					<i class="publicIcon code-i"></i>
				</div>
				<!-- <div class="sideBarUserLogin">
					
				</div> -->
			</div>
			<!--回到顶部-->
			<div class="rSideBarItem backTop">
				<div class="rsItemName">
					<i class="publicIcon backTop-i"></i>
				</div>
				<!-- <div class="sideBarUserLogin">
					
				</div> -->
			</div>
	   </div>
	</div>
	<!--右侧导航结束-->