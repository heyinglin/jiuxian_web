$(function(){
	//导航特效开始
	$('.top-main .main-right .jiuxian .jiuxiantop').mouseenter(function(){	//鼠标进入.jiuxian时
		// alert(1);
		$(this).css({'background':'#fff'});	/*鼠标移上去改变背景色*/
		$(this).next().show();	/*将盒子显示出来,注意这里不能将父元素带上，否则没有效果*/
		$(this).find('span i.caret').css({'transform':'rotate(180deg)'});	/*将小三角旋转180度*/
	});
	$('.top-main .main-right .jiuxian').mouseleave(function(){	//鼠标离开.jiuxian时
		$(this).find('.jiuxiantop').css({'background':'#F2F2F2'});	//鼠标离开时换回原来的颜色
		$(this).find('.mydropdown').css({'display':'none'});	//鼠标离开时隐藏盒子
		$(this).find('span i.caret').css({'transform':'rotate(0deg)'});	/*小三角复位*/
	});
	//导航特效结束

	//搜索框特效的开始
	$('.search-form').val('超级单品日');		//给文本框的value设置值
	$('.search-form').focus(function(){		//文本框获得焦点时，清空文本框中的值
		$('.search-form').val('');
	});	
	$('.search-form').blur(function(){		//文本框失去焦点时，恢复文本框中的值
		$('.search-form').val('超级单品日');
	});	
	//搜索框特效结束

	//幻灯片上方的导航特效开始
	$('.nav .nav-main .center ul li').first().css({'background':'#B40C10'});
	$('.nav .nav-main .center ul li').mouseenter(function(){
		$('.nav .nav-main .center ul li:not(.first)').css({'background':'#990000'});
		$(this).css({'background':'#B40C10'});
	});
	$('.nav .nav-main .center ul li').mouseleave(function(){
		$('.nav .nav-main .center ul li:not(.first)').css({'background':'#990000'});
	});
	//幻灯片上方的导航特效结束

	//幻灯片插件特效(最大的)
	$('#theTarget').skippr();

	//左侧导航特效
	var width=$('.leftmenu li').width();
	var x=$('.leftmenu li').position().left+width;	//js中，+号首先是加，而不是字符串拼接
	var y=$('.leftmenu li').position().top;
	$('.leftmenu li').mouseenter(function(){
		$(this).find('.leftnav').show();
		$(this).css({'background':'#F1F1F1'});
		$(this).find('.rightdiv').show().css({'left':x+'px','top':y+'px'});
	});
	$('.leftmenu li').mouseleave(function(){
		$(this).find('.leftnav').hide();
		$(this).css({'background':'#fff'});
		$(this).find('.rightdiv').hide();
	});
	/*左侧导航特效结束*/
	

	//中间左边部分标签页的实现
	$('.contentFirst .indexTabBox .indexTabNav ul li').mouseenter(function(){
		$('.contentFirst .indexTabBox .indexTabNav ul li').removeClass('on');
		$(this).addClass('on');
		// $(this).css({'cursor':'pointer'});
		var idx=$(this).index();
		// alert(idx);
		$('.contentFirst .indexTabBox .indexTabConWrap').hide();
		$('.contentFirst .indexTabBox .indexTabConWrap').eq(idx).show();
	});
	$('.contentFirst .indexTabBox .indexTabNav ul li').mouseleave(function(){
		$('.contentFirst .indexTabBox .indexTabNav ul li').removeClass('on');
	});
	$('.contentFirst .indexTabBox .indexTabConWrap').first().show();
	//中间左边部分标签页的结束

	//中间右边部分标签页的开始
	$('.indexTabRight .indexTabNew .indexTabNewCon').first().show();
	$('.indexTabRight .indexTabNew .indexTabNewNav ul li').mouseenter(function(){
		$(this).addClass('on');
		var id=$(this).index();
		// alert(id);
		$('.indexTabNewCon').hide();
		$('.indexTabNewCon').eq(id).show();
	});
	$('.indexTabRight .indexTabNew .indexTabNewNav ul li').mouseleave(function(){
		$('.indexTabRight .indexTabNew .indexTabNewNav ul li').removeClass('on');
	});
	//中间右边部分标签页的结束

	//标签页右边的上方幻灯片的实现
	$('#theTarget2').skippr();
	//标签页右边的下方幻灯片的实现
	$('#theTarget3').skippr();

	//中间优惠部分幻灯片的实现
	//上方三个小正方形的的划定效果
	$('.titleBox .rightNavBox span').click(function(){
		$('.titleBox .rightNavBox span').removeClass('on');
		$(this).addClass('on');
		var idx=$(this).index();
		s=-v*idx
		// $('.raceListBox .receBoxs').css({'margin-left':s+'px'});
		$('.raceListBox .receBoxs').animate({'margin-left':s+'px'},600);
	});
	/*$('.titleBox .rightNavBox span').mouseleave(function(){
		$('.titleBox .rightNavBox span').removeClass('on');
	});*/
	//上方三个小正方形的的划定效果结束
	/*$('.raceListBox i.raceLeft').mouseenter(function(){
		var obj=$(this).position();
		alert(obj.left);
	});*/
	//开始实现幻灯片
	var s=0;
	var v=1188;	//6个li标签的宽
	$('.raceListBox i.raceRight').click(function(){
		// alert(1);
		// $('.raceListBox .receBoxs').css({'margin-left':'-1200px'});
		s-=v;
		var left=-2376;	//12个li标签的宽
		if(s<=left){
			s=left;
		}
		// $('.raceListBox .receBoxs').css({'margin-left':s+'px'});
		$('.raceListBox .receBoxs').animate({'margin-left':s+'px'},600);
		var m=0
		m=Math.abs(s/v);	//abs()返回数的绝对值
		$('.titleBox .rightNavBox span').removeClass('on');
		$('.titleBox .rightNavBox span').eq(m).addClass('on');
	});
	$('.raceListBox i.raceLeft').click(function(){
		// alert(1);
		// $('.raceListBox .receBoxs').css({'margin-left':'-1200px'});
		s+=v;
		if(s>=0){
			s=0;
		}
		// $('.raceListBox .receBoxs').css({'margin-left':s+'px'});
		$('.raceListBox .receBoxs').animate({'margin-left':s+'px'},600);
		var n=0
		n=Math.abs(s/v);	//abs()返回数的绝对值
		$('.titleBox .rightNavBox span').removeClass('on');
		$('.titleBox .rightNavBox span').eq(n).addClass('on');
	});
	//中间优惠部分幻灯片的结束

	//左侧滚动监听
	/*一楼图标动态效果的实现*/
	$('.floorOne').mouseenter(function(){
		$(this).find('.floorOneOn').show().animate({'width':'100px'},800);
	});
	$('.floorOne').mouseleave(function(){
		$(this).find('.floorOneOn').show().animate({'width':'30px'},800,function(){
			$('.floorOne .floorOneOn').hide();
		});
	});
	/*二楼图标动态效果的实现*/
	$('.floorTwo').mouseenter(function(){
		$(this).find('.floorTwoOn').show().animate({'width':'100px'},800);
	});
	$('.floorTwo').mouseleave(function(){
		$(this).find('.floorTwoOn').show().animate({'width':'30px'},800,function(){
			$('.floorTwo .floorTwoOn').hide();
		});
	});
	/*三楼图标动态效果的实现*/
	$('.floorThree').mouseenter(function(){
		$(this).find('.floorThreeOn').show().animate({'width':'100px'},800);
	});
	$('.floorThree').mouseleave(function(){
		$(this).find('.floorThreeOn').show().animate({'width':'30px'},800,function(){
			$('.floorThree .floorThreeOn').hide();
		});
	});
	/*四楼图标动态效果的实现*/
	$('.floorFour').mouseenter(function(){
		$(this).find('.floorFourOn').show().animate({'width':'100px'},800);
	});
	$('.floorFour').mouseleave(function(){
		$(this).find('.floorFourOn').show().animate({'width':'30px'},800,function(){
			$('.floorFour .floorFourOn').hide();
		});
	});
	/*五楼图标动态效果的实现*/
	$('.floorFive').mouseenter(function(){
		$(this).find('.floorFiveOn').show().animate({'width':'100px'},800);
	});
	$('.floorFive').mouseleave(function(){
		$(this).find('.floorFiveOn').show().animate({'width':'30px'},800,function(){
			$('.floorFive .floorFiveOn').hide();
		});
	});
	$('.floorBack').mouseenter(function(){
		$(this).find('.floorBackArrow').css({'background-position':'-95px -190px'})
		// alert(1);
	});
	$('.floorBack').mouseleave(function(){
		$(this).find('.floorBackArrow').css({'background-position':'-74px -190px'})
		// alert(1);
	});
	/*回到顶部功能的实现*/
	$('.floorBack').click(function(){
		$(window).scrollTop(0);
		$('.fixDiv').fadeOut(300);
	});
	/*回到顶部功能的结束*/
	/*楼层滚动监听的是实现*/
	/*$(window).scroll(function(){	//页面滚动条滚动事件，当页面滚动条滚动时候，自动触发
		if($(window).scrollTop()>300){
			$('.fixDiv').fadeIn(600);
		}
		else{
			$('.fixDiv').hide();
		}
	});*/

	/*var arr=[];	//定义一个空数组，用来放每一个楼层相对于document对象的垂直方向的偏移量
	$('.jxFloor').each(function(i){		//i是函数执行的次数
		var h=$(this).offset().top;
			arr[i]=h;
	});

	$(window).scroll(function(){	//滚动条滚动的时候，触发页面滚动事件
		var xh=$(window).scrollTop();
		var m=0;
		for(var i=0;i<=arr.length;i++){
			if(xh>arr[i]){
			m=i;
		}
		switch(m)
		{
			case 0:
			// alert(i);
			var name=$('.fixFloor').eq(m).find('.floorOn').attr('name');
			$('.floorFix').eq(m).find('.floorOn').html(name);
			#code...;
			break;
			case 1:
			// alert(i);
			var name=$('.fixFloor').eq(m).find('.floorOn').attr('name');
			$('.floorFix').eq(m).find('.floorOn').html(name);
			#code...;
			break;
			case 2:
			// alert(i);
			var name=$('.fixFloor').eq(m).find('.floorOn').attr('name');
			$('.fixFloor').eq(m).find('.floorOn').html(name);
			#code...;
			break;
			case 3:
			// alert(i);
			var name=$('.fixFloor').eq(m).find('.floorOn').attr('name');
			$('.fixFloor').eq(m).find('.floorOn').html(name);
			#code...;
			break;
			case 4:
			// alert(i);
			var name=$('.fixFloor').eq(m).find('.floorOn').attr('name');
			$('.fixFloor').eq(m).find('.floorOn').html(name);
			#code...;
			break;
			default:
			#code...;
			break;
		}
	}
	});*/
	/*楼层滚动监听的结束*/

	/*右侧导航的实现*/
	/*测量有效屏幕的高度*/
	// alert($(window).height());
	$('.rSideBarItem').mouseenter(function(){
		$(this).css({'background':'#c00'});
		$(this).find('span').css({'color':'#fff'});
	});
	$('.rSideBarItem').mouseleave(function(){
		$(this).css({'background':'#fff'});
		$(this).find('span').css({'color':'#000'});
	});
	var top=$('.sideBarUserLogin').position().top;
	// alert(top);
	$('.user').mouseenter(function(){
		$('.sideBarUserLogin').css({'display':'block','top':top+'px'});
	});
	$('.user').mouseleave(function(){
		$('.sideBarUserLogin').hide();
	});
	/*一楼白酒馆幻灯片的实现*/
	$('.btnBox .btnBg em').click(function(){
		// alert(1);
		$('.btnBox .btnBg em').removeClass('on');
		$(this).addClass('on');
		var idex=$(this).index();
		switch(idex){
			case 0:
			// $('.sliderBox .imgBox').css({'margin-left':0+'px'});
			$('.sliderBox .imgBox').animate({'margin-left':0+'px'},600);
			break;
			case 1:
			$('.sliderBox .imgBox').animate({'margin-left':-210+'px'},600);
			break;
			case 2:
			$('.sliderBox .imgBox').animate({'margin-left':-420+'px'},600);
			break;
			case 3:
			$('.sliderBox .imgBox').animate({'margin-left':-630+'px'},600);
			break;
		}
	});

	/*二楼白酒馆幻灯片的实现*/
	/*三楼白酒馆幻灯片的实现*/
	/*四楼白酒馆幻灯片的实现*/
	/*五楼白酒馆幻灯片的实现*/

	/*一楼底部标签页的实现*/
	var dis=985;
	$('.whiteWine .topTenWrap .topTenBox .topTenNavBox .topTenNav a').mouseenter(function(){
		var id=$(this).index();
		switch(id){
			case 0:
			// $('.sliderBox .imgBox').css({'margin-left':0+'px'});
			$('.whiteWine .topTenWrap .topTenBox .topTenConWrap .topTenCon').animate({'margin-left':'0px'},600);
			break;
			case 1:
			$('.whiteWine .topTenWrap .topTenBox .topTenConWrap .topTenCon').animate({'margin-left':'-985px'},600);
			break;
			case 2:
			$('.whiteWine .topTenWrap .topTenBox .topTenConWrap .topTenCon').animate({'margin-left':'-1970px'},600);
			break;
			case 3:
			$('.whiteWine .topTenWrap .topTenBox .topTenConWrap .topTenCon').animate({'margin-left':'-2955px'},600);
			break;
		}
		// alert(1);
		// $('.whiteWine .topTenWrap .topTenBox .topTenConWrap .topTenCon').animate({'margin-left':'-985px'},600);
	});
	/*二楼底部标签页的实现*/
	$('.grapeWine .topTenWrap .topTenBox .topTenNavBox .topTenNav a').mouseenter(function(){
		var id=$(this).index();
		switch(id){
			case 0:
			// $('.sliderBox .imgBox').css({'margin-left':0+'px'});
			$('.grapeWine .topTenWrap .topTenBox .topTenConWrap .topTenCon').animate({'margin-left':'0px'},600);
			break;
			case 1:
			$('.grapeWine .topTenWrap .topTenBox .topTenConWrap .topTenCon').animate({'margin-left':'-985px'},600);
			break;
			case 2:
			$('.grapeWine .topTenWrap .topTenBox .topTenConWrap .topTenCon').animate({'margin-left':'-1970px'},600);
			break;
			case 3:
			$('.grapeWine .topTenWrap .topTenBox .topTenConWrap .topTenCon').animate({'margin-left':'-2955px'},600);
			break;
		}
		});
		/*三楼底部标签页的实现*/
		$('.foreignWine .topTenWrap .topTenBox .topTenNavBox .topTenNav a').mouseenter(function(){
		var id=$(this).index();
		switch(id){
			case 0:
			// $('.sliderBox .imgBox').css({'margin-left':0+'px'});
			$('.foreignWine .topTenWrap .topTenBox .topTenConWrap .topTenCon').animate({'margin-left':'0px'},600);
			break;
			case 1:
			$('.foreignWine .topTenWrap .topTenBox .topTenConWrap .topTenCon').animate({'margin-left':'-985px'},600);
			break;
			case 2:
			$('.foreignWine .topTenWrap .topTenBox .topTenConWrap .topTenCon').animate({'margin-left':'-1970px'},600);
			break;
			case 3:
			$('.foreignWine .topTenWrap .topTenBox .topTenConWrap .topTenCon').animate({'margin-left':'-2955px'},600);
			break;
		}
		});
		/*四楼底部标签页的实现*/
		$('.sundryWine .topTenWrap .topTenBox .topTenNavBox .topTenNav a').mouseenter(function(){
		var id=$(this).index();
		switch(id){
			case 0:
			// $('.sliderBox .imgBox').css({'margin-left':0+'px'});
			$('.sundryWine .topTenWrap .topTenBox .topTenConWrap .topTenCon').animate({'margin-left':'0px'},600);
			break;
			case 1:
			$('.sundryWine .topTenWrap .topTenBox .topTenConWrap .topTenCon').animate({'margin-left':'-985px'},600);
			break;
			case 2:
			$('.sundryWine .topTenWrap .topTenBox .topTenConWrap .topTenCon').animate({'margin-left':'-1970px'},600);
			break;
			case 3:
			$('.sundryWine .topTenWrap .topTenBox .topTenConWrap .topTenCon').animate({'margin-left':'-2955px'},600);
			break;
		}
		});
		/*五楼底部标签页的实现*/
		$('.food .topTenWrap .topTenBox .topTenNavBox .topTenNav a').mouseenter(function(){
		var id=$(this).index();
		switch(id){
			case 0:
			// $('.sliderBox .imgBox').css({'margin-left':0+'px'});
			$('.food .topTenWrap .topTenBox .topTenConWrap .topTenCon').animate({'margin-left':'0px'},600);
			break;
			case 1:
			$('.food .topTenWrap .topTenBox .topTenConWrap .topTenCon').animate({'margin-left':'-985px'},600);
			break;
			case 2:
			$('.food .topTenWrap .topTenBox .topTenConWrap .topTenCon').animate({'margin-left':'-1970px'},600);
			break;
			case 3:
			$('.food .topTenWrap .topTenBox .topTenConWrap .topTenCon').animate({'margin-left':'-2955px'},600);
			break;
		}
		});	

		/*官方推荐滑动条start*/
		$('.mt_1 .titleBox ul li').mouseenter(function(){	//.titleBox和上面的名字重复，所以要限制好
			var left=$(this).position().left;
			// alert(left);
			// $('.titleSlider').css({'left':left+'px'});
			$('.mt_1 .titleSlider').animate({'left':left+'px'},200);
			var id=$(this).index();
			// alert(id);
			$('.mt_1 .logoBox .logoAll').eq(id).css({'display':'block'}).siblings('.mt_1 .logoBox .logoAll').css({'display':'none'});
		});
		$('.mt_1 .logoBox .optionsID span.prevPage').click(function(){
			$('.mt_1 .logoBox .logoAll .logoFirst .logoFirstBd').animate({'margin-left':'0px'},600);
			// alert('1');
		});
		$('.mt_1 .logoBox .optionsID span.nextPage').click(function(){
			$('.mt_1 .logoBox .logoAll .logoFirst .logoFirstBd').animate({'margin-left':'-1185px'},600);
			// alert('1');
		});
		$('.mt_1 .logoBox .logoAll').first().css({'display':'block'}).siblings('.mt_1 .logoBox .logoAll').css({'display':'none'});
		/*官方推荐滑动条end*/

		/*官方推荐产品滑动start*/
		
		/*官方推荐产品滑动end*/
});