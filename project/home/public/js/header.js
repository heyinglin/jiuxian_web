$(function(){
	//导航特效开始
	$('.top-main .main-right .jiuxian .jiuxiantop').mouseenter(function(){	//鼠标进入.jiuxian时
		// alert(1);
		$(this).css({'background':'#fff'});	/*鼠标移上去改变背景色*/
		$(this).next().show();	/*将盒子显示出来,注意这里不能将父元素带上，否则没有效果*/
		$(this).find('span i.caret').css({'transform':'rotate(180deg)'});	/*将小三角旋转180度*/
	});
	$('.top-main .main-right .jiuxian').mouseleave(function(){	//鼠标离开.jiuxian时
		$(this).find('.jiuxiantop').css({'background':'#F2F2F2'});	//鼠标离开时换回原来的颜色
		$(this).find('.mydropdown').css({'display':'none'});	//鼠标离开时隐藏盒子
		$(this).find('span i.caret').css({'transform':'rotate(0deg)'});	/*小三角复位*/
	});
	//导航特效结束

	//搜索框特效的开始
	$('.search-form').val('超级单品日');		//给文本框的value设置值
	$('.search-form').focus(function(){		//文本框获得焦点时，清空文本框中的值
		$('.search-form').val('');
	});	
	$('.search-form').blur(function(){		//文本框失去焦点时，恢复文本框中的值
		$('.search-form').val('超级单品日');
	});	
	//搜索框特效结束

	//幻灯片上方的导航特效开始
	$('.nav .nav-main .center ul li').first().css({'background':'#B40C10'});
	$('.nav .nav-main .center ul li').mouseenter(function(){
		$('.nav .nav-main .center ul li:not(.first)').css({'background':'#990000'});
		$(this).css({'background':'#B40C10'});
	});
	$('.nav .nav-main .center ul li').mouseleave(function(){
		$('.nav .nav-main .center ul li:not(.first)').css({'background':'#990000'});
	});
	//幻灯片上方的导航特效结束

	//左侧导航特效
	$('.leftmenu').hide();
	$('.nav-main h2').mouseenter(function(){
		$('.nav-main .leftmenu').show();
		$('.nav-main h2').css({'cursor':'pointer'});
	});
	$('.nav-main .left').mouseleave(function(){
		$('.leftmenu').hide();
	});
	/*var width=$('.leftmenu li').width();
	var x=$('.leftmenu li').position().left+width;	//js中，+号首先是加，而不是字符串拼接
	var y=$('.leftmenu li').position().top;*/
	$('.leftmenu li').mouseenter(function(){
		$(this).find('.leftnav').show();
		$(this).css({'background':'#F1F1F1'});
		// $(this).find('.rightdiv').show().css({'left':x+'px','top':y+'px'});
	});
	$('.leftmenu li').mouseleave(function(){
		$(this).find('.leftnav').hide();
		$(this).css({'background':'#fff'});
		$(this).find('.rightdiv').hide();
	});
	/*左侧导航特效结束*/

	/*右侧导航的实现*/
	/*测量有效屏幕的高度*/
	// alert($(window).height());
	$('.rSideBarItem').mouseenter(function(){
		$(this).css({'background':'#c00'});
		$(this).find('span').css({'color':'#fff'});
	});
	$('.rSideBarItem').mouseleave(function(){
		$(this).css({'background':'#fff'});
		$(this).find('span').css({'color':'#000'});
	});
	var top=$('.sideBarUserLogin').position().top;
	// alert(top);
	$('.user').mouseenter(function(){
		$('.sideBarUserLogin').css({'display':'block','top':top+'px'});
	});
	$('.user').mouseleave(function(){
		$('.sideBarUserLogin').hide();
	});
	/*右侧导航的结束*/
});